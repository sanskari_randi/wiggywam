
 var Agency = require("../models/agency.model");
 var Agent = require("../models/agent.model");
 var FAQ = require("../models/faq.model");
 var sendSignupEmail = require("../controllers/sendMail");

 var config = require("../../config");
 var secretKey = config.secretKey;
//


var express =  require('express');
var jsonwebtoken = require('jsonwebtoken');

var router = express.Router();


function createToken(user) {
    var token = jsonwebtoken.sign({
        password:user.password,
        email: user.email
    }, secretKey, {
        expiresIn: 86400
    });
    return token;
};

// route middleware that will happen on every request
router.use(function(req, res, next) {

    // log each request to the console
    console.log(req.method, req.url);
    console.log('Time: ', Date.now());

    // continue doing what we were doing and go to the route
    next();
});
  // create new agency api
router.post('/createagency', function(req, res){
        var agency = new Agency({
            agencyName:  req.body.agencyName,
            primaryContact: req.body.primaryContact,
            email: req.body.email,
            phoneNumber: req.body.phoneNumber
        });
        agency.save(function(err){
            if(err){
                res.send(err);
                return;
            }
            else {
              let randomPassword = Math.random().toString(36).substr(2, 6);
               var agent = new Agent({
                 email: req.body.email,
                 phoneNumber: req.body.phoneNumber,
                 fullName: req.body.primaryContact,
                 password: randomPassword,
                 agency: agency._id
               })
               agent.save(function(err){
                 if(err){
                   res.send(err)
                 } else {
                   sendSignupEmail.sendSignupEmail(req.body.email, randomPassword);
                 }
               })
               res.json({message: 'Agency has been created', success:true,
                    success: true,
                    agency:agency});
}
        });

    })

    .post('/signup', function(req, res){
        var agent = new Agent({
            email: req.body.email,
            password: req.body.password
        });
        var token = createToken(agent);

        agent.save(function(err){
            if(err){
                res.send(err);
                return;
            }
            res.json({token: token,
                message: 'User has been created', success:true,
                success: true,
                agent:agent});
        });
    })


        .post('/login', function (req, res) {
            Agent.findOne({
                'email': req.body.email
            }).select('email password').exec(function (err, agent) {
                if(err) throw err;
                if(!agent){
                    res.send({message:' User does not exist'})
                } else {
                    console.log("i am here");
                    console.log(agent.password);
                    console.log(req.body.password);
                    var savedpass = agent.password;
                    var providedpass = req.body.password;
                    if(savedpass == providedpass){
                      var token = createToken(agent);
                      res.json({
                          success:true,
                          message : "Successfully Loggedin!",
                          token: token,
                          user:agent
                      });
                    }
                    else {
                      res.send({message: "Invalid Password"});
                    }
                }
            });
        })

    // creating agent under specific agency api
      .post('/createagent/:agencyID', function(req,res){
        randomPassword = Math.random().toString(36).substr(2, 6);
          var agent = new Agent({
            firstName:  req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            phoneNumber: req.body.phoneNumber,
            password: randomPassword,
            agency: req.params.agencyID
          });

          agent.save(function(err){
              if(err){
                  res.send(err);
                  return;
              }
              sendSignupEmail.sendSignupEmail(req.body.email, randomPassword);
              res.json({message: 'wwAgent has been created', success:true,
                  success: true});
          });

      })

      // total number of api agency
      .get('/agencycount', function(req,res){
          Agency.count({}, function(err, count){
            if(err){
              res.send(err);
            }
            console.log(count);
            res.json({count:count});
          });
        })

        //agents under perticular agency api
      .get('/allagent/:agencyId', function(req,res){
        Agent.find({agency:req.params.agencyId}, function(err,agents){
          if(err){
            res.send(err);
          } else {
            res.json(agents);
          }
        });
      })

      //single agent api
      .get('/agent/:agentid', function(req,res){
        Agent.findById({_id: req.params.agentid}, function(err,agent){
          if(err){
            res.send(err);
          }
          res.json(agent)
        });

      })
      // total number of agent api
      .get('/wwagentcount',function(req,res){
        Agent.count({}, function(err, count){
          if(err){
            res.send(err);
          }
          console.log(count);
          res.json({count:count});
        });
      })

      // block unblock api
      .put('/block/:agentid',function(req,res){
        Agent.findById(req.params.agentid, function(err,agent){
          if(err){
            res.send(err);
          }
          else{
            if(agent.status==='unblocked'){
              agent.status = 'blocked';
              agent.save(function(err){
                if(err) res.send(err)
                res.json({message:"agent status updated",
              agent:agent})
              })
            }
            else {
              agent.status = 'unblocked';
              agent.save(function(err){
                if(err) res.send(err)
                res.json({message:"agent status updated",
                agent:agent
              })
              })
            }
          }
        })
      })

      // FAQ api
      .post('/faq/:id',function(req,res){
        var faq = new FAQ({
          question: req.body.question,
          answer: req.body.answer,
          creator: req.params.id,
        });
        faq.save(function(err){
          if(err){
            res.send(err);
            return;
          }
          else{
            res.json({message: "FAQ done",
          faq:faq});
          }
        })
      })

module.exports = router;

//         .post('/login', function (req, res) {
//             Agent.findOne({
//                 email: req.body.email
//             }).select('email password').exec(function (err, user) {
//                 if(err) throw err;
//                 if(!user){
//                     res.send({message:' User does not exist'})
//                 } else if(user){
//                     var validPassword = user.comparePassword(req.body.password);
//
//                     if(!validPassword){
//                         res.send({message: "Invalid Password"});
//                     } else {
//                         // then create a token
//
//                         var token = createToken(user);
//                         res.json({
//                             success:true,
//                             message : "Successfully Loggedin!",
//                             token: token
//                         });
//                     }
//                 }
//             });
//         });
//
//
//     api.use(function (req, res, next) {
//         console.log("Somebody just come to our app!");
//
//         var token = req.body.token || req.param('token') ||  req.headers['x-access-token']
//
//         if(token){
//             jsonwebtoken.verify(token, secretKey, function (err, decoded) {
//                 if(err){
//                     res.status(403).send({success: false, message:'Failed to authenticate user'});
//                 } else{
//                     req.decoded = decoded;
//                     next();
//                 }
//             });
//         } else{
//                 res.status(403).send({success: false, message: 'no Token provided'});
//         }
//     });
//
//     // routes that needs to be authenticated using above middleware
//
//
//     api.get('/me', function(req, res){
//       console.log("hello");
//         res.json("hello");
//     });
//
//     return api;
