var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AgencySchema = new Schema({
    // we need the creator ID and its in user schema so we are referencing it
    //creator: {type: Schema.Types.ObjectId, ref: 'User'},
    agencyName: String,
    primaryContact: String,
    email: String,
    phoneNumber: Number,
    userType: {type: String, default: "agency"},
    created: {type:Date, default:Date.now}
});

module.exports = mongoose.model('Agency', AgencySchema);
