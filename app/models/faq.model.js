var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var FAQSchema = new Schema({
    // we need the creator ID and its in user schema so we are referencing it
    //creator: {type: Schema.Types.ObjectId, ref: 'User'},
    question: String,
    answer: String,
    creator: {type: Schema.Types.ObjectId, ref: 'Agent'},
    created: {type:Date, default:Date.now}
});

module.exports = mongoose.model('FAQ', FAQSchema);
