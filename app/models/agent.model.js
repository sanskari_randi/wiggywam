var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AgencySchema = new Schema({
    // we need the creator ID and its in user schema so we are referencing it
    //creator: {type: Schema.Types.ObjectId, ref: 'User'},
    firstName: String,
    lastName: String,
    email: {type: String, unique: true},
    phoneNumber: Number,
    password: String,
    agency: {type: Schema.Types.ObjectId, ref: 'Agency'},
    status: {type: String, default: "unbloked"},
    userType: {type: String, default: "wwAgent"},
    created: {type:Date, default:Date.now}
});

module.exports = mongoose.model('Agent', AgencySchema);
