var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var config = require('./config');

var router = require("./app/routes/api");

mongoose.connect(config.db ,function (err) {
    if(err)
    {
        console.log(err);
    } else{
        console.log('connected to MongoDB!!');
    }
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));



app.use('/api',router);

//app.use(express.static(__dirname + '/public'));


app.listen(config.PORT);
console.log('Magic happens on port ' + config.PORT);
