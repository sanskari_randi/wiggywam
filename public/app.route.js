angular.module('appRoutes', ['ngRoute'])

.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

      $routeProvider
          .when('/', {
              templateUrl: '/views/agency.html',
              controller: 'agencyController',
              controllerAs: 'agencyCtrl'
          })
          .when('/accountManager', {
              templateUrl: '/views/account.manager.html',
              controller: 'accountManagerController',
              controllerAs: 'accountCtrl'
          })
          .when('/feedback', {
              templateUrl: '/views/feedback.html',
              controller: 'feedbackController',
              controllerAs: 'feedbackCtrl'
          })
          .when('/video', {
              templateUrl: '/views/video.html',
              controller: 'videoController',
              controllerAs: 'videoCtrl'
          })
          .when('/faq', {
              templateUrl: '/views/faq.html',
              controller: 'faqController',
              controllerAs: 'faqCtrl'
          })
          .when('/login', {
              templateUrl: '/views/login.html',
              controller: 'loginController',
              controllerAs: 'loginCtrl'
          })

          .otherwise('/');
          $locationProvider.html5Mode(true);
    }]);
